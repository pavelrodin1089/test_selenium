import allure
import pytest
import time
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options


class TestFirstSteps:
    @allure.feature('Open site')
    @allure.story('Просто переходим на страницу искомого сайта')
    def test_wrong_credentials(self):
        print("This is test")
        with allure.step("Инициализация драйвера"):
            service = Service()
            options = Options()
            options.add_argument("--no-sandbox")
            options.add_argument('--headless')
            options.add_argument("--disable-gpu")
            driver = webdriver.Chrome(service=service, options=options)
        with allure.step("Переход на страницу ГосУслуг"):
            driver.get("https://www.gosuslugi.ru/")
            time.sleep(1)

